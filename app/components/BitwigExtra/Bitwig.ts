import * as _ from 'underscore'
var Base64 = require('js-base64').Base64;

export namespace Bitwig {
    export interface EQBand {
        freq: number,
        q: number,
        amp: number,
        on: boolean,
        type: number
    }
    export interface EQPacket {
        bands: EQBand[]
    }
    export interface TrackPacket {
        volume: number,
        volumeString: string,
        pan: number,
        name: string,
        color: string,
        type: 'standard' | 'group',
        index: number,
        solo: boolean,
        mute: boolean,
        eq: EQPacket
    }
    export interface SavedData {
        tags: TagData[]
    }
    export interface TagData {
        tag: string,
        start: string
    }
}

let connected = false

const onMessage: Function[] = []
export function onMessageReceived(callback) {
    onMessage.push(callback)
}

declare const TextDecoder: any;

export function post(resource: any, modifier?: any) {
    const toSend = `<header>${JSON.stringify({ resource, modifier })}<footer>`
    function sendPart(toSend) {
        var arr = new ArrayBuffer(4); // an Int32 takes 4 bytes
        var view = new DataView(arr);
        const length = toSend.length //new TextEncoder('utf-8').encode(toSend).length
        view.setUint32(0, length, false); // byteOffset = 0; litteEndian = false
        const prependedSizeByteString = new TextDecoder('utf-8').decode(new Uint32Array(arr))
        // console.log(toSend)
        ws.send(Base64.encode(prependedSizeByteString + toSend))
    }
    var remaining = toSend
    while (remaining.length > 0) {
        sendPart(remaining.substr(0, 50))
        remaining = remaining.substr(50)
    }
}


export function getLatestLevels() {
    return latestLevels
}
let latestLevels: number[] = new Array()
latestLevels.fill(0)
const levelCallbacks: { [id: string]: Function } = {};
let nextId = 0
export function onLevels(callback: Function) {
    const id = nextId++
    levelCallbacks[id] = callback
    return {
        off() {
            delete levelCallbacks[id]
        }
    }
}
const ws = new WebSocket("ws://localhost:8887", "base64");
ws.onmessage = (event) => {
    if (!connected) {
        this.connected = true
        post({ 'data': 'currentProject' })
    }
    const data = atob(event.data);
    try {
        const separator = "<MESSAGEHEADERFUCKYOU>"
        const messages = data.split(separator)
        messages.forEach(function (message) {
            message = message.trim()
            if (message.length === 0) {
                return
            }
            const parsed = JSON.parse(message);
            //console.log(parsed);
            onMessage.forEach(function (callback) {
                callback(parsed)
            })

            if (parsed.type === 'levels') {
                latestLevels = parsed.data
                _.values(levelCallbacks).forEach(function (callback) {
                    callback(parsed.data)
                })
            }
        })

    } catch (e) {
        console.error(e)
        console.log(data)
    }

}