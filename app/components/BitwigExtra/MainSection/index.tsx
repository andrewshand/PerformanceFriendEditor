import { TrackGroupAudible } from '../TrackGroupAudible';
import * as React from 'react';
import * as style from './style.css';
import { TrackComponent } from '../TrackComponent';
import { post, onMessageReceived, Bitwig, getLatestLevels } from '../Bitwig'
import { getTagglessNameForTrack, getTrackTags, isAvailableTag } from '../tags';
import { Slider } from 'react-toolbox/lib/slider';
import * as _ from 'underscore';
import * as classnames from 'classnames';
var StickyDiv = require('react-stickydiv');

export namespace MainSection {
  export interface Props { }
  export interface State {
    searching: boolean,
    openTagGroups: { [key: string]: 'manual' | 'auto' },
    eqing: boolean,
    searchTerm: string,
    tracks: Bitwig.TrackPacket[];
    selectedTracks: any,
    randomiseBounds: {
      min: number,
      max: number
    },
    saveData: Bitwig.SavedData,
    availableTags: Set<string>,
    loopingTag?: string
  }
}

export class MainSection extends React.Component<MainSection.Props, MainSection.State> {

  selectedTrackDomNode?: HTMLElement
  tracksByName: { [name: string]: Bitwig.TrackPacket } = {}
  previousSelections: any[] = []
  previousSelectionIndexFromBack = 0
  transportPos: number = 0
  receivedInitialData: boolean = false
  timeSigResult: number = 4

  constructor(props?: MainSection.Props, context?: any) {
    super(props, context);
    this.state = {
      tracks: [],
      searching: false,
      searchTerm: '',
      selectedTracks: {},
      randomiseBounds: {
        min: 0.5,
        max: 0.88
      },
      saveData: { tags: [] },
      eqing: false,
      openTagGroups: {},
      availableTags: new Set()
    };

    onMessageReceived((message) => {
      if (message.type === 'tracks') {
        // update all tracks
        this.setState((state) => {
          state.tracks = message.data
          this.tracksByName = _.indexBy(state.tracks, 'name')
          return state
        })
        this.updateAvailableTags()
      }
      if ('resource' in message) {
        if (message.resource === 'transport') {
          const timeSig = message.modifier.timeSignature
          const [numerator, denominator] = timeSig.split('/')
          this.timeSigResult = parseInt(numerator, 10) / parseInt(denominator, 10)
          this.transportPos = message.modifier.position / (this.timeSigResult * 4)
          return
        }
        if ('data' in message.resource) {
          if (message.modifier) {
            this.setState(state => {
              state.saveData = JSON.parse(message.modifier)
              return state
            })
          }
          this.receivedInitialData = true
          return
        }
        if ('track' in message.resource) {
          // update an individual track by index
          const index = this.getTrackIndexFromSelector(message.resource)
          if (this.state.tracks[index]) {
            // only update a track we know about
            this.setState((state) => {
              state.tracks[index] = Object.assign({}, state.tracks[index], message.modifier)
              return state
            })
            this.updateAvailableTags()
          }
        }
      }
    })
  }
  updateAvailableTags() {
    this.setState(state => {
      state.availableTags = new Set(Object.keys(this.getTracksGroupedByTag()))
      return state
    })
  }
  getTrackIndexFromSelector(selector: any) {
    if (selector.track.index) {
      return selector.track.index
    }
    return this.tracksByName[selector.track.name].index
  }
  toggleTagGroupOpen(tagGroup) {
    this.setState(state => {
      if (tagGroup in state.openTagGroups) {
        delete state.openTagGroups[tagGroup]
      }
      else {
        state.openTagGroups[tagGroup] = 'manual'
      }
      return state
    })
  }

  componentWillMount() {
    window.addEventListener("keydown", this.handleKeyDown.bind(this));
    window.addEventListener("keyup", this.handleKeyUp.bind(this));
  }

  getDuplicatedTagsAndTimes() {
    return _.flatten(this.state.saveData.tags.map((el, index, array) => {
      if (el.tag.toLowerCase() === 'all' || !this.state.availableTags.has(el.tag)) {
        return []
      }
      return el.start.split(',').map(function (time) {
        const t = parseInt(time, 10)
        if (isNaN(t)) {
          return []
        }
        return [
          {
            tag: el.tag,
            start: t
          }
        ]
      })
    })).sort((a, b) => a.start - b.start)
  }

  componentDidMount() {
    setInterval(() => {
      // update currently open group
      const duplicateForEachTime = this.getDuplicatedTagsAndTimes()
      duplicateForEachTime.some((tag, index, array) => {
        const nextTag = array[index + 1]
        const start = tag.start

        if (!nextTag || this.transportPos >= start - 1 && this.transportPos < nextTag.start - 1) {
          // set this to the current group
          let newOpenTagGroups = Object.assign({}, this.state.openTagGroups)
          Object.keys(newOpenTagGroups).forEach(key => {
            if (newOpenTagGroups[key] === 'auto') {
              delete newOpenTagGroups[key]
            }
          })
          newOpenTagGroups[tag.tag] = 'auto'
          if (Object.keys(newOpenTagGroups).join('') === Object.keys(this.state.openTagGroups).join('')) {
            // save setting state
          }
          else {
            // this.setState(state => {
            //   state.openTagGroups = newOpenTagGroups
            //   return state
            // })
          }
          return true
        }
        return false
      })
    }, 100)
    setInterval(() => {
      // save data
      if (!this.receivedInitialData) {
        return
      }
      post({ data: 'currentProject' }, JSON.stringify(this.state.saveData, null, 4))
    }, 10000)
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.handleKeyDown.bind(this));
    window.removeEventListener("keyup", this.handleKeyUp.bind(this));
  }

  handleKeyUp(event: any) {
    if (event.keyCode === 69) {
      // e
      this.setState(state => {
        state.eqing = false
        return state
      })
    }
  }

  handleKeyDown(event: any) {
    if (event.metaKey) {
      if (event.keyCode === 70) { // cmd + f
        event.preventDefault();
        event.stopPropagation();
        this.setState(state => {
          state.searching = true;
          return state;
        })
        this.forceUpdate(); // make sure if we press cmd+f again we get focused
      }
      else if (!event.shiftKey && event.keyCode === 90) { // cmd + z
        event.preventDefault();
        event.stopPropagation();
        post({ 'app': 'undo' })
      }
      else if (event.shiftKey && event.keyCode == 90) { // cmd + shift + z
        event.preventDefault();
        event.stopPropagation();
        post({ 'app': 'redo' })
      }
    }

    if (event.keyCode === 32) { // spacebar
      post({ app: 'play/pause' })
      event.preventDefault()
      event.stopPropagation()
    }
    if (event.keyCode === 27) {
      this.setState(state => {
        state.searching = false;
        return state;
      })
    }

    if (this.previousSelections.length > 0) {
      if (event.keyCode === 219 && this.previousSelectionIndexFromBack < this.previousSelections.length - 1) {
        // selection history backwards
        this.setState(state => {
          this.previousSelectionIndexFromBack++
          state.selectedTracks = this.previousSelections[this.previousSelections.length - (this.previousSelectionIndexFromBack)]
          return state
        })
      }

      if (event.keyCode === 221 && this.previousSelectionIndexFromBack > 0) {
        // selection history forward
        this.setState(state => {
          this.previousSelectionIndexFromBack--
          state.selectedTracks = this.previousSelections[this.previousSelections.length - 1 - (this.previousSelectionIndexFromBack)]
          return state
        })
      }
    }

    if (event.keyCode === 69 && !this.state.eqing) {
      // e
      this.setState(state => {
        state.eqing = true
        return state
      })
    }
    // if (event.keyCode === 74 || event.keyCode === 81) {
    //   // j or q
    //   post({ app: 'rewind' })
    // }
    // if (event.keyCode === 76 || event.keyCode === 69) {
    //   // l or e
    //   post({ app: 'fastForward' })
    // }
    if (event.keyCode === 65) {
      // left (a)
      this.navigateTracks('left')
    }
    if (event.keyCode === 68) {
      // right (d)
      this.navigateTracks('right')
    }
    if (event.keyCode === 87) {
      // up (w)
      this.navigateTracks('up')
    }
    if (event.keyCode === 83) {
      // down (s)
      this.navigateTracks('down')
    }
  }

  onSearchKeyUpImpl = _.debounce(function (event: any) {
    const searchTerm = event.target.value.trim()
    this.setState((state) => {
      state.searchTerm = searchTerm
      return state
    })
  }, 100)

  onSearchKeyDown(event: any) {
    event.stopPropagation()
  }

  onSearchKeyUp(event: any) {
    event.persist()
    if (event.keyCode === 32) {
      // prevent space from pausing
      event.stopPropagation()
    }
    if (event.keyCode === 27) {
      // escape
      return this.setState(state => {
        state.searching = false;
        return state;
      })
    }
    this.onSearchKeyUpImpl(event)
  }

  navigateTracks(direction: 'up' | 'down' | 'left' | 'right', secondAttempt = false) {

    const selectWithDomNode = (node?: HTMLElement) => {
      if (!node) {
        return
      }
      const { index, tag } = node.dataset
      const track = this.state.tracks[index]
      if (track) {
        this.selectTrack(track, tag)
        node.scrollIntoView()
        window.scrollBy(0, -50)
      }
    }

    const getValidElement = (x: number, y: number): HTMLElement => {
      const el = document.elementFromPoint(x, y) as HTMLElement
      if (!el) {
        return null
      }
      const withTrack = el.dataset.name ? el : el.closest('[data-name]') as HTMLElement
      return withTrack
    }
    const pos = this.selectedTrackDomNode.getBoundingClientRect()
    const offset = 2

    if (direction === 'up') {
      const above = getValidElement(pos.left + pos.width / 2, pos.top - offset) as HTMLElement
      if (above) {
        selectWithDomNode(above)
      }
      else if (!secondAttempt) {
        window.scrollTo(window.scrollX, window.scrollY - pos.height * 2)
        this.navigateTracks('up', true)
      }
    } else if (direction === 'left') {
      selectWithDomNode(getValidElement(pos.left - offset, pos.top + pos.height - offset) as HTMLElement)
    } else if (direction === 'right') {
      selectWithDomNode(getValidElement(pos.left + pos.width + offset, pos.top + pos.height - offset) as HTMLElement)
    } else if (direction === 'down') {
      const el = getValidElement(pos.left + pos.width / 2, pos.top + pos.height + offset) as HTMLElement
      if (el) {
        selectWithDomNode(el)
      }
      else if (!secondAttempt) {
        window.scrollTo(window.scrollX, window.scrollY + pos.height * 2)
        this.navigateTracks('down', true)
      }
    }
  }

  renderSearch() {
    return <div className={style.search}>
      <input onKeyDown={this.onSearchKeyDown.bind(this)} onKeyUp={this.onSearchKeyUp.bind(this)} className={style.searchInput} type="search" autoFocus placeholder="Start typing a track name" />
    </div>
  }

  getFilteredTracks() {
    return this.state.tracks.filter((track) => {
      if (this.state.searching && this.state.searchTerm.trim().length > 0) {
        return track.name.toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) !== -1
      }
      return true;
    })
  }

  getTracksGroupedByTag() {
    const tracks = this.getFilteredTracks()
    let buckets: any = { All: { tracks, audible: false } }
    tracks.forEach(function (track) {
      const tags = getTrackTags(track)
      tags.forEach(function (tag) {
        buckets[tag] = buckets[tag] || { tracks: [], audible: false }
        buckets[tag].tracks.push(track)
        if (getLatestLevels()[track.index] > 0) {
          buckets[tag].audible = true
        }
      })
    })
    return buckets
  }

  getUsedTags() {
    return
  }

  handleOnClick(event) {
    event.preventDefault()
    event.stopPropagation()
    return false
  }

  randomise() {
    const selectedTrackNames = Object.keys(this.state.selectedTracks)
    const indexes = selectedTrackNames.map((name) => {
      return this.tracksByName[name].index
    })
    const range = this.state.randomiseBounds.max - this.state.randomiseBounds.min
    indexes.forEach((index) => {
      const volume = this.state.randomiseBounds.min + (Math.random() * range)
      post({ track: { index } }, { volume })
    })
  }

  handleRandomiseMinMaxChange(type: 'min' | 'max', value: number) {
    this.setState(state => {
      state.randomiseBounds[type] = value
      return state
    })
  }
  renderMultiselectOptions() {
    return <div className={style.multiselectOptions}>
      <div className={style.randomise}>
        <div className={style.min}>Min</div>
        <Slider
          value={this.state.randomiseBounds.min}
          min={0}
          max={1}
          theme={style}
          onChange={this.handleRandomiseMinMaxChange.bind(this, 'min')}
        />
        <div className={style.max}>Max</div>
        <Slider
          value={this.state.randomiseBounds.max}
          min={0}
          max={1}
          theme={style}
          onChange={this.handleRandomiseMinMaxChange.bind(this, 'max')}
        />
        <div className={style.button} onClick={this.randomise.bind(this)}>Randomise</div>
      </div>
    </div>
  }
  storeSelection(selection: any) {
    this.previousSelections = this.previousSelections.slice(-50).concat([selection])
    this.previousSelectionIndexFromBack = 0
  }
  deselectTrack(track, inGroup: string) {
    this.setState(state => {
      delete state.selectedTracks[track.name]
      if (Object.keys(state.selectedTracks).length !== 0) {
        this.storeSelection(state.selectedTracks)
      }
      return state
    })
  }
  getStartsForTag(tag: string): string {
    const found = this.state.saveData.tags.find(tagData => tagData.tag === tag)
    return found ? found.start : '0'
  }
  getStartForTag(tag: string) {
    const found = this.state.saveData.tags.find(tagData => tagData.tag === tag)
    if (found) {
      const parsed = parseInt(found.start.split(',')[0], 10)
      if (!isNaN(parsed)) {
        return parsed
      }
    }
    return 0
  }
  selectTrack(track, inGroup: string, addToCurrentSelection: boolean = false) {
    if (this.state.selectedTracks[track] === inGroup) {
      // don't need to run this if the track is already selected
      return
    }

    this.setState(state => {
      if (addToCurrentSelection) {
        state.selectedTracks[track.name] = inGroup
      }
      else {
        state.selectedTracks = { [track.name]: inGroup }
      }
      this.storeSelection(state.selectedTracks)
      return state
    })

    const query = `[data-name="${getTagglessNameForTrack(track)}"][data-tag="${inGroup}"]`
    this.selectedTrackDomNode = document.querySelector(query) as HTMLElement
    post({ track: { index: track.index } }, { selected: true })
  }
  onTagStartClick(event: any) {
    event.preventDefault()
    event.stopPropagation()
  }
  onTagStartChange(tag: string, event: any) {
    event.stopPropagation()
    let value = event.target.value
    this.setState(state => {
      state.saveData.tags = state.saveData.tags.filter(tagData => tagData.tag !== tag)
      state.saveData.tags.push({ tag, start: value })
      return state
    })
  }
  trackGroupSorter(a, b) {
    if (a.toLowerCase() === 'All') {
      return 1
    }
    return this.getStartForTag(a) - this.getStartForTag(b)
  }
  loopTagToggle(tag: string, event: any) {
    event.stopPropagation()
    const tagInfo = this.state.saveData.tags.find(tagData => tagData.tag === tag)
    if (tagInfo) {
      if (this.state.loopingTag === tag) {
        post({ transport: "" }, { loop: [] })
        this.setState(state => {
          state.loopingTag = null
          return state
        })
      }
      else {
        let found: any = null
        const tagsNTimes = this.getDuplicatedTagsAndTimes()
        const counts = _.countBy(tagsNTimes, 'tag')
        tagsNTimes.some((currentTag, index, array) => {
          const nextTag = array[index + 1]
          const start = currentTag.start
          if (tag !== currentTag.tag) {
            return
          }

          const hasMultiple = counts[currentTag.tag] > 1
          if (!hasMultiple) {
            if (nextTag) {
              found = { tag, start: currentTag.start - 1, end: nextTag.start - 1 }
              return true
            }
            else {
              found = { tag, start: currentTag.start - 1, end: currentTag.start - 1 + (this.timeSigResult * 8 * 4) }
              return true
            }
          }
          if (nextTag && this.transportPos >= start && this.transportPos < nextTag.start) {
            found = { tag, start: currentTag.start - 1, end: nextTag.start - 1 }
            return true
          }
          // if the next one if the last one, then we'll definitely use it
          counts[currentTag.tag]--
        })

        if (found) {
          post({ transport: "" }, { loop: [found.start * this.timeSigResult * 4, found.end * this.timeSigResult * 4] })
          this.setState(state => {
            state.loopingTag = tag
            return state
          })
        }
      }
    }
  }
  render() {
    const { searching, selectedTracks } = this.state;
    const groupedByTag = this.getTracksGroupedByTag()
    const multipleSelected = Object.keys(selectedTracks).length > 1
    const self = this
    return (
      <div onContextMenu={this.handleOnClick.bind(this)} className={style.main} onKeyDown={this.handleKeyDown.bind(this)}>
        <div className={style.trackList}>
          {_.keys(groupedByTag).sort(this.trackGroupSorter.bind(this)).map(trackGroup => {
            const trackGroupVisible = trackGroup in this.state.openTagGroups || this.state.searching
            const { tracks } = groupedByTag[trackGroup]
            const isLooping = this.state.loopingTag === trackGroup

            return <div className={style.trackGroup} key={trackGroup}>
              <StickyDiv zIndex={2} key={trackGroup} className={style['-sticky']}>
                <div onClick={this.toggleTagGroupOpen.bind(this, trackGroup)} className={classnames(style.trackGroupHeader, style[trackGroupVisible ? '-visible' : '-hidden'])}>
                  <span className={style.trackGroupName}>{trackGroup}</span>
                  <span className={style.trackGroupCount}>({tracks.length})</span>
                  {trackGroup.toLowerCase() === 'all'
                    ? null
                    : <input className={style.trackGroupStart} value={this.getStartsForTag(trackGroup)} onKeyDown={this.onTagStartChange.bind(this, trackGroup)} onClick={this.onTagStartClick.bind(this)} onChange={this.onTagStartChange.bind(this, trackGroup)} />}
                  {trackGroup.toLowerCase() === 'all'
                    ? null
                    : <span onClick={this.loopTagToggle.bind(this, trackGroup)} className={classnames(style.trackGroupLooping, isLooping ? style['-looping'] : null)}><i className="fa fa-repeat" aria-hidden="true"></i></span>}
                  <span><TrackGroupAudible tracks={tracks} /></span>
                </div>
              </StickyDiv>
              {
                trackGroupVisible ? <div key={trackGroup + "-tracks"} className={style.groupTracks}>
                  {tracks.map((track) => {
                    return <TrackComponent
                      multipleSelected={multipleSelected}
                      availableTags={_.keys(groupedByTag).filter(isAvailableTag)}
                      key={track.name + trackGroup}
                      track={track}
                      selectTrack={self.selectTrack.bind(this, track, trackGroup)}
                      deselectTrack={self.deselectTrack.bind(this, track, trackGroup)}
                      selected={self.state.selectedTracks[track.name] === trackGroup}
                      inTag={trackGroup}
                      eqing={this.state.eqing}
                    />
                  })}
                </div> : null
              }
            </div>
          })}
        </div>
        {searching ? this.renderSearch() : null}
        {multipleSelected ? this.renderMultiselectOptions() : null}
      </div>
    );
  }
}
