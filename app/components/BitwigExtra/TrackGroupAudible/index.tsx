import { isUndefined } from 'util';
import * as React from 'react';
import * as classNames from 'classnames';
import * as style from './style.css';
import { applyMiddleware } from 'redux';
import { MainSection } from '../MainSection'
import Knob from 'react-canvas-knob';
import { Slider } from 'react-toolbox/lib/slider';
import { Bitwig, post, onLevels, getLatestLevels } from '../Bitwig'
import * as _ from 'underscore';
import * as classnames from 'classnames';
import { getTrackTags, getTagglessName, TAG_CHAR, isUnusableTag } from '../tags'

export namespace TrackGroupAudible {
    export interface Props {
        tracks: Bitwig.TrackPacket[]
    }

    export interface State {
        audible: boolean
    }
}

export class TrackGroupAudible extends React.Component<TrackGroupAudible.Props, TrackGroupAudible.State> {
    audibleTimer: any
    constructor(props?: TrackGroupAudible.Props, context?: any) {
        super(props, context);
        this.state = {
            audible: false
        };
    }
    isAudible() {
        return this.props.tracks.some(track => {
            return (getLatestLevels()[track.index] / 128) > 0.25
        })
    }
    componentWillMount() {
        this.audibleTimer = setInterval(() => {
            this.setState(state => {
                state.audible = this.isAudible()
                return state
            })
        }, 1700)
    }
    componentWillUnmount() {
        clearInterval(this.audibleTimer)
    }
    render() {
        return <span className={classnames(style.volume, this.state.audible ? style['-audible'] : null)} style={{ width: '2rem', display: 'block', textAlign: 'right' }}><i className="fa fa-volume-down"></i></span>
    }
}
