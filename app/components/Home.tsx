import { getTagglessName } from './BitwigExtra/tags';
import * as React from 'react';
let { dialog } = require('electron').remote
import { remote } from 'electron'
const { BrowserWindow } = remote
import * as fs from 'fs';
import { onMessageReceived, Bitwig } from './BitwigExtra/Bitwig'
import * as _ from 'underscore';
import * as Autosuggest from 'react-autosuggest';
interface Suggestion {
  value: string,
  name: string
}
interface State {
  filePath: string,
  data: Data,
  tracks: Bitwig.TrackPacket[],
  suggestions: Suggestion[]
}
interface Event {
  type: "arm",
  trackName: string,
  beat: number,
  device: 'Mic' | 'Keyboard' | 'Guitar'
}
interface Data {
  events: Event[]
}
let styles = require('./Home.scss');

export default class Home extends React.Component<any, State> {

  tracksByName: any = {}

  constructor(props: any) {
    super(props)
    this.state = {
      tracks: [],
      filePath: '',
      data: {
        events: []
      },
      suggestions: []
    }

    onMessageReceived(event => {
      if (event.type === 'tracks') {
        // update all tracks
        this.setState((state) => {
          state.tracks = event.data
          this.tracksByName = _.indexBy(state.tracks, 'name')
          return state
        })
      }
    })
  }

  getTrackSuggestionsForValue(inputValue: string) {
    if (!inputValue) {
      return []
    }
    return _.keys(this.tracksByName).map(trackName => {
      const tagless = getTagglessName(trackName)
      return {
        name: tagless,
        value: trackName
      }
    }).filter(suggestion => {
      return suggestion.name.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0
    })
  }

  componentDidMount() {
    dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), { properties: ['openFile'] }, file => {
      const path = file.toString()
      const data = fs.readFileSync(path).toString()
      this.setState(state => {
        state.data = JSON.parse(data)
        state.filePath = path
        return state
      })
    });
  }

  onKeyDown(event: any) {
    if (event.metaKey && event.keyCode === 83) { // cmd + s
      const backupDate = new Date().getTime()
      fs.renameSync(this.state.filePath, this.state.filePath.substr(0, this.state.filePath.length - '.json'.length) + backupDate + '.json')
      fs.writeFileSync(this.state.filePath, JSON.stringify(this.state.data, null, 4))
    }
  }

  onRemove(index: number, event: any) {
    this.setState(state => {
      state.data.events.splice(index, 1)
      return state
    })
  }

  onInsert(index: number, event: any) {
    this.setState(state => {
      const duplicate = Object.assign({}, state.data.events[index])
      state.data.events.splice(index, 0, duplicate || {
        beat: 0,
        type: 'arm',
        trackName: '',
        device: 'Keyboard'
      })
      return state
    })
  }

  updateEvent(index: number, update: any) {
    this.setState(state => {
      Object.assign(this.state.data.events[index], update)
      this.state.data.events = this.state.data.events.sort((a, b) => {
        return a.beat - b.beat
      })
      return state
    })
  }

  onTrackNameChange(eventIndex: number, event, { newValue }) {
    this.updateEvent(eventIndex, { trackName: newValue })
  }

  onEventBeatChange(eventIndex: number, event: any) {

    const value = parseFloat(event.target.value)
    if (isNaN(value)) {
      return
    }

    this.updateEvent(eventIndex, {
      beat: value
    })
  }

  onDeviceChange(eventIndex: number, event: any) {
    const value = event.target.value
    this.updateEvent(eventIndex, {
      device: value
    })
  }

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getTrackSuggestionsForValue(value)
    })
  }

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  renderSuggestion(suggestion: Suggestion, { query, isHighlighted }) {
    return <div className={styles.suggestion + ' ' + (isHighlighted ? styles['-highlighted'] : '')}>{suggestion.name}</div>
  }
  renderEvents() {
    return this.state.data.events.map((event, index) => {
      const inputProps = {
        placeholder: 'track',
        value: event.trackName,
        onChange: this.onTrackNameChange.bind(this, index)
      }
      return <div className={styles.event} key={index}>
        <div className="remove" onClick={this.onRemove.bind(this, index)}>Remove</div>
        <div className="insert" onClick={this.onInsert.bind(this, index)}>Insert</div>
        <input placeholder="beat" onChange={this.onEventBeatChange.bind(this, index)} value={event.beat} />
        <input placeholder="device" onChange={this.onDeviceChange.bind(this, index)} value={event.device} />
        <Autosuggest
          suggestions={this.state.suggestions}
          getSuggestionValue={value => value.value}
          inputProps={inputProps}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          renderSuggestion={this.renderSuggestion}
        />
      </div>
    })
  }
  render() {

    return (
      <div onKeyDown={this.onKeyDown.bind(this)}>
        <div className={styles.container} data-tid="container">
          {this.renderEvents()}
        </div>
        <div className="insert" onClick={this.onInsert.bind(this, 0)}>Insert</div>
      </div>
    );
  }
}
